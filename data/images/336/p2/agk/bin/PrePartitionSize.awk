BEGIN {nextline = 0;}
{
  if (nextline == 1)
  {
    print($2 * 512);
    nextline = 0;
  }
  else if ($1 == "Device")
  {
    nextline = 1;
  }
}
