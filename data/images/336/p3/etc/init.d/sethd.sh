#!/bin/sh

DRIVE=${1}

if [ -b ${DRIVE} ]; then

  ISATA=$(hdparm $DRIVE|grep ATA)
  if [ "$ISATA" == "" ] ; then
	hdparm -r 1 -a 4 -c 1 -m 0 -u 1           ${DRIVE} 2>&1; # simple
  else
	hdparm -r 1 -a 4 -c 1 -m 1 -u 1 -A 1 -P 8 ${DRIVE} 2>&1; # fancy
  fi
# keep for reference 
#      => this performance boost hangs some legacy flash chips
# # get all of the hdparm flags into environment variables
  eval $(for P in $(hdparm -i ${DRIVE} |grep , | sed -e "s/, / /g" ); \
          do echo $P | grep = ; done)
 
  echo MaxMultSect=${MaxMultSect}
 
# hdparm -m $((${MaxMultSect} + 0)) ${DRIVE} 2>&1

fi

