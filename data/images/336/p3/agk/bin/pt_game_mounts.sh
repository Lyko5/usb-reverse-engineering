# no magic we are "sourced"

if [ -L /bin/mount ]; then
  # if mount is a link busybox mount
  BINDOPT="-o bind"
else
  # else it is a standard linux mount
  BINDOPT="--bind"
fi

/bin/mount -t proc none /vgmroot/proc
/bin/mount -t usbfs none /vgmroot/proc/bus/usb
/bin/mount ${BINDOPT} /nvram /vgmroot/nvram
/bin/mount ${BINDOPT} /nvram_os /vgmroot/nvram_os
/bin/mount ${BINDOPT} /agk /vgmroot/agk
/bin/mount ${BINDOPT} /dev /vgmroot/dev
/bin/mount ${BINDOPT} /initrd /vgmroot/initrd
/bin/mount ${BINDOPT} /initrd/tmp/Alpha6000 /vgmroot/tmp 

/bin/mount ${BINDOPT} /games /vgmroot/games
if [ -d /games/EXTRA_FILES ]; then
  /bin/mount ${BINDOPT} /games/EXTRA_FILES /vgmroot/games/EXTRA_FILES
fi

unset BINDOPT

export DO_GAME_CHROOT=Y
export GAME_CHROOT_DIR=/vgmroot

