#!/bin/sh

# 300MHz    00:02.0 Class 0200: 1282:9102 (rev 40)

# 1GHz      02:08.0 Class 0200: 8086:103a (rev 82)

# find the ethernet device

echo "starting ethernet init"

DEVICE=$(grep 'Class 0200:' /tmp/LSPCI.txt )
if [ "${DEVICE}" != "" ]; then
  ID=$(echo ${DEVICE} | cut -d ' ' -f4)
  echo ID=\"${ID}\"

  if   [ "${ID}" = "1282:9102" ]; then
    # Kontron 300MHz 
    DRIVERLIST="dmfe "
  elif [ "${ID}" = "8086:103a" ]; then
    # Intel
    DRIVERLIST="eepro100 "
  elif [ "${ID}" = "1106:0102" ]; then
    DRIVERLIST="mii via-rhine "
  else
    echo UNKOWN ETHERNET DEVICE ID=\"${ID}\" 
  fi
  for MOD in ${DRIVERLIST}; do
    MODPATH=$(find /lib/modules/2.4.18-3pt/kernel/drivers/net -name ${MOD}.o 2>/dev/null)
    if [ "${MODPATH}" != "" ]; then
      insmod ${MODPATH} >>/tmp/rc.sysinit.log
    else
      RESULTS="${RESULTS} : ethernet missing ${MOD}"
    fi
  done
fi

export RESULTS

