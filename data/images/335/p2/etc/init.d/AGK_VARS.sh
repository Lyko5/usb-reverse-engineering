#
# this "script" to source configures the constants for the Alpha Game Kit
#
# it is used by /initrd/linuxrc /etc/rc.sysinit and /etc/profile

gethostip(){

if [ "$1" = "" ]; then
  SEARCH="/dev/root"
else
  SEARCH="$1"
fi

# find our root nfs device
  HOSTIP=$(cat /proc/mounts | grep "$SEARCH" | grep "nfs")

# pull out the host name if there is one
  HOSTIP="$(echo "$HOSTIP" | cut -d "=" -f,4 | cut -d " " -f,1)"

# does it have an IP address in it?
  HOSTIP="$(echo "$HOSTIP" | grep "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*")"

# Return what ever is left
  echo "$HOSTIP"
}

# Convert the Linux kernel command line parameters into exports
for CMDPARM in $(cat /proc/cmdline ); do
  if [ "${CMDPARM%=*}" != "${CMDPARM}" ]; then
    eval "export ${CMDPARM}"
  fi
done

# are we an nfs root flash?
CMDLINE="$(grep nfsroot= /proc/cmdline)"
if [ "$CMDLINE" != "" ]; then
  echo ================== nfsroot ====================================

  for part in $CMDLINE; do
    case $part in

    nfsroot=*)
      # parse out the remote directory to mount as our root
      NFSROOT="${part##*=}"
      echo found NFSROOT=$NFSROOT
    ;;

    ip=*)
      # parse out network information
      IPSTRING="${part##ip=}"
      MYIP=$(   echo $IPSTRING | cut -d ":" -f1); echo -n ' 'MYIP=$MYIP
      HOSTIP=$( echo $IPSTRING | cut -d ":" -f2); echo -n ' 'HOSTIP=$HOSTIP
      BROADCAST="10.10.2.255"                   ; echo -n ' 'BROADCAST=$BROADCAST
      NETMASK=$(echo $IPSTRING | cut -d ":" -f4); echo -n ' 'NETMASK=$NETMASK
      MYNAME=$( echo $IPSTRING | cut -d ":" -f5); echo -n ' 'MYNAME=$MYNAME
      echo
    ;;

    *)
      # ignore every thing else for now
    ;;

    esac

  done
  echo ===============================================================

  hostname $MYNAME; hostname

else

  echo ================== ${root} ===================================
  echo "Check for new validation"

  if [ "${root}" != "/dev/hda6" ]; then
    export AGKVALIDATION="ENABLED"
  else
    export AGKVALIDATION=""
  fi
fi

export UNAME=$(cat /proc/version|cut -d " " -f3)

# Test for VARIOUS AGK I/O Board AND Cabinet things
#
#$PROGRESS Looking for Configuration

export AGKBIOSENABLE="$(hq -o 0x08000 8)"
export AGKASIC="$(hq -a)"
export AGKCAB="$(hq -c)"
export AGKBRDREV="$(hq -i)"
export AGKMFG="$(hq -m)"
export AGKPLATFORM="$(hq -p)"

# are we a new sfl system with bios_flags on the cmdline?
if [ "${bios_flags}" != "" ]; then 
   export AGKBIOSENABLE="${bios_flags}" 
fi

if [ "$AGKBIOSENABLE" =  "0000000000000003" ]; then
  export AGKUPLOAD="1"
fi

export AGKDOWNLOAD="build-selected"
export AGKBOOTSTRAP=""

# VIDEO CONSTANTS
export AGKVIDEO="SVGA 640 480"

# MODE CONSTANTS
export AGKSIM=""

# IP CONTANTS
export HOSTIP="$(gethostip)"
export AGKIP="$(/sbin/ifconfig eth0 2>/dev/null | grep inet | cut -d ":" -f,2 | cut -d " " -f,1)"

