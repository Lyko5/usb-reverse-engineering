#!/bin/sh

LoadModule() {
local INSMODRESULT=$1
local RESULT=255
echo "Try loading Module: $1"
  if [ -f $1 ]; then
    INSMODRESULT=$(modprobe $(basename $1 .o))
    RESULT=$?
  fi
  echo RESULT=$RESULT INSMODRESULT=\"$INSMODRESULT\" $1 >&1
  return $RESULT
}

# Load more-or-less-standard kernel modules
#
for mod in sdgmodules/agkmod.ko \
           sdgmodules/ipc.ko \
           sdgmodules/jiffies.ko \
           sdgmodules/8250_pci.ko \
   ; do

    LoadModule /lib/modules/$UNAME/$mod  >>/tmp/rc.sysinit.log

done

