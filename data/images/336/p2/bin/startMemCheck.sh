#!/bin/bash
# ============================================================================
# Copyright (c) 1999-2007 Bally Technologies
#
# This script is used to create the device node for the memcheck loadable module.
#
#
# Date:    Author:     Changes:
# -------- ----------- -------------------------------------------------------
# 07-06-07 R. Cadima  Initial release
# ============================================================================
# Find the module and insmod it
#

# Find the module in the loaded devices and create a dev entry for it
#
 
MAJOR=$(grep memcheck /proc/devices|gawk '{print($1)}')
if [ "$MAJOR" == "" ] ; then
	echo "Unable to find memcheck device"
	export MACHINE_FAULT="-e 0x00010000 No memory validation module"
else
	# Remove any old node for memcheck driver and create new one
	rm -rf /dev/memcheck 2>/dev/null
	mknod /dev/memcheck c $MAJOR 0
	echo "Major number of memory debug module is $MAJOR"

	# Start the process ot initiate the memory validation support.
	background /bin/validate-memory
fi
