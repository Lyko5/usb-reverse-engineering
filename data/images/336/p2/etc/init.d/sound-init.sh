#!/bin/sh

# 300MHz    00:07.5 Class 0401: 1106:3058 (rev 50)

# 1GHz      00:1f.5 Class 0401: 8086:24c5 (rev 02)

# find the MultiMedia Audio device

echo "starting sound init"
DRIVERPATH=/lib/modules/$UNAME/kernel/sound
INTEL_AC97_LIST="i810_audio "
VIA_AC97_LIST="via82cxxx_audio "

DEVICE=$(grep ' 0401:' /tmp/LSPCI.txt )
if [ "${DEVICE}" != "" ]; then
  ID=$(echo ${DEVICE} | cut -d ' ' -f3)
  echo ID=\"${ID}\"

  if   [ "${ID}" = "1106:3058" ]; then
    # Older VIA chip set (Kontron 400Mhz?)
    DRIVERLIST="$VIA_AC97_LIST"
  elif [ "${ID}" = "1106:3059" ]; then
     # Newer Kontron 300MHz 
    DRIVERLIST="$VIA_AC97_LIST"
  elif [ "${ID}" = "8086:24c5" ]; then
    # Intel
    DRIVERLIST="$INTEL_AC97_LIST"
  else
    echo UNKOWN SOUND DEVICE ID=\"${ID}\" 
  fi

  # all sound cards need this loaded
  modprobe soundcore
  modprobe ac97_codec

  for MOD in ${DRIVERLIST}; do
    MODPATH=$(find ${DRIVERPATH} -name ${MOD}.ko 2>/dev/null)
    if [ "${MODPATH}" != "" ]; then
      insmod ${MODPATH} >>/tmp/rc.sysinit.log
    else
      RESULTS="${RESULTS} : sound missing ${MOD}"
    fi
  done
fi

export RESULTS


