#!/bin/bash
#
# ntpd		This shell script takes care of starting and stopping
#		ntpd (NTPv4 daemon).
#
# chkconfig: - 58 74
# description: ntpd is the NTPv4 daemon. \
# The Network Time Protocol (NTP) is used to synchronize the time of \
# a computer client or server to another server or reference time source, \
# such as a radio or satellite receiver or modem.

# Source function library.

OPTIONS="-p /var/run/ntpd.pid"

ntpconf=/etc/ntp.conf

[ -x /usr/sbin/ntpd -a -f $ntpconf ] || exit 0

RETVAL=0
prog="/usr/sbin/ntpd"

start() {
	timeservers=`head -n 1 $ntpconf | sed -e 's/server \(.*\)/\1/g'`

        if [ -z "$tickers" ]; then
	    tickers=$timeservers
        fi

	# Synchronize with servers if step-tickers exists
	# or the -x option is used
	/usr/sbin/ntpdate -s -b -p 8 $tickers
	RETVAL=$?
	echo $"$prog: Synchronizing with time server: $RETVAL"
	if [ $RETVAL -ne 0 ]; then
		OPTIONS="$OPTIONS -g"
	fi

        # Start daemons.
        /usr/sbin/ntpd $OPTIONS
	RETVAL=$?
        echo $"Starting $prog: $RETVAL"
	return $RETVAL
}

stop() {
        # Stop daemons.
	kill -9 `cat /var/run/ntpd.pid`
	RETVAL=$?
        echo $"Shutting down $prog: $RETVAL"
	return $RETVAL
}

# See how we were called.
case "$1" in
  start)
	start
        ;;
  stop)
	stop
        ;;
  restart|reload)
	stop
	start
	RETVAL=$?
	;;
  *)
        echo $"Usage: $0 {start|stop|restart}"
        exit 1
esac

exit $RETVAL
