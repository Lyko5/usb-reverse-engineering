#!/bin/sh

# process video Class controller(s) we detect in a special order

# Determine if we using an Nvidia add on card
# We look for the PCI Vendor ID for Nvidia, which is 10de
insmod /lib/modules/$UNAME/kernel/drivers/char/agp/agpgart.ko
if   [ "$(grep ".*0300:.*10de:" /tmp/LSPCI.txt)" != "" ]; then
   echo using an NVIDIA video card
   NVIDIA="1"
   S3VIDEO=""
   ln -fs /etc/vga/libvga.config.vesa /nvram/libvga.config
   insmod /lib/modules/$UNAME/kernel/drivers/char/agp/nvidia-agp.ko

   . /nvidia.173.14.12/detectmon.sh

# Determine if we using an Intel onboard video chip
# We look for the PCI Vendor ID for Intel, which is 8086
elif [ "$(grep ".*0300:.*8086:" /tmp/LSPCI.txt)" != "" ]; then
   echo using Intel onboard video
   NVIDIA=""
   S3VIDEO=""
   export SVGALIB_CONFIG_FILE=/nvram/libvga.config
   insmod /lib/modules/$UNAME/kernel/drivers/char/agp/intel-agp.ko

# Determine if we using an S3 onboard video chip or the VIA chip
# We look for the PCI Vendor ID for S3, which is 5333
elif [ "$(grep ".*0300:.*5333:" /tmp/LSPCI.txt)" != "" ]; then
   echo using Savage onboard video
   NVIDIA=""
   S3VIDEO="$(grep ".*0300:.*5333:" /tmp/LSPCI.txt)"
   export SVGALIB_CONFIG_FILE=/nvram/libvga.config
   insmod /lib/modules/$UNAME/kernel/drivers/char/agp/via-agp.ko

# assume that anything else will just work with a vesa driver
else
   echo using a default vesa video system
   NVIDIA=""
   S3VIDEO=""
   export SVGALIB_CONFIG_FILE=/nvram/libvga.config
   insmod /lib/modules/$UNAME/kernel/drivers/char/agp/intel-agp.ko

fi

export  NVIDIA="${NVIDIA}" 
export S3VIDEO="${S3VIDEO}"

echo "NVIDIA ="\"${NVIDIA}\"
echo "S3VIDEO="\"${S3VIDEO}\"

echo AGKSCREENB=${AGKSCREENB}
eval $(/nvidia.173.14.12/resconfig)

# if video width or height unknown then the default is 800 x 600
if [ -z ${AGKVIDEOWIDTH}  ]; then export AGKVIDEOWIDTH="800";  fi
if [ -z ${AGKVIDEOHEIGHT} ]; then export AGKVIDEOHEIGHT="600"; fi

echo "AGKVIDEOWIDTH ="${AGKVIDEOWIDTH}
echo "AGKVIDEOHEIGHT="${AGKVIDEOHEIGHT}

# Set up the link for the libvga.config file (savage or vesa)(high or low res).
if [ "$NVIDIA" == "" ]; then     # no NVIDIA card.

  if [ "$S3VIDEO" == "" ]; then   # use VESA svga.
    if [ "${AGKVIDEOWIDTH}" == "800" ] && [ "${AGKVIDEOHEIGHT}" == "600" ]; then
      #use the libvga.config.vesa file with the lower HorizSync.
      ln -fs /etc/vga/libvga.config.vesa.lowres /nvram/libvga.config
    else 
      #use the libvga.config.vesa file with the higher HorizSync.
      ln -fs /etc/vga/libvga.config.vesa /nvram/libvga.config
    fi

  elif [ "$S3VIDEO" != "" ]; then # use SAVAGE svga.
    if [ "${AGKVIDEOWIDTH}" == "800" ] && [ "${AGKVIDEOHEIGHT}" == "600" ]; then
      #use the libvga.config.savage file with the lower HorizSync.
      ln -fs /etc/vga/libvga.config.savage.lowres /nvram/libvga.config
    else 
      #use the libvga.config.savage file with the higher HorizSync.
      ln -fs /etc/vga/libvga.config.savage /nvram/libvga.config
    fi
  fi
fi 

# show the user which  libvga.config we're using.
ls -l /nvram/libvga.config | cut -d '/' -f3-


# If nVIdia card present, we start X

if [ "${NVIDIA}" = "1" ] ; then
  echo "running X_init.sh"
  insmod /lib/modules/$UNAME/kernel/drivers/i2c/i2c-core.ko
  insmod /lib/modules/$UNAME/kernel/misc/nvidia.ko
  . /nvidia.173.14.12/X_init.sh
  # setup faultmgr video config, for non-OGL wait til video starts.
  if [ "${AGKVIDEO}" = "OGL" ] || [ "${AGKVIDEO}" = "OGL32R" ]; then
     /agk/bin/cmdfaultdog -v
  fi
fi

