# Reference Documents

## Sniffer Device

[Bugblat | ezSniffer USB Protocol Analyzer](https://www.bugblat.com/products/ezsniff/index.html)

## Python Code

[PyUSB Package](https://github.com/pyusb/pyusb)

[PyUSB Tutorial](https://github.com/pyusb/pyusb/blob/master/docs/tutorial.rst)

[USB Communication with Python and PyUSB](http://mvidner.blogspot.com/2017/01/usb-communication-with-python-and-pyusb.html)

[USB Reverse Engineering: Down the rabbit hole](https://www.devalias.net/devalias/2018/05/13/usb-reverse-engineering-down-the-rabbit-hole/)
