#!/bin/sh

# 300MHz    00:07.5 Class 0401: 1106:3058 (rev 50)

# 1GHz      00:1f.5 Class 0401: 8086:24c5 (rev 02)

# find the MultiMedia Audio device

echo "starting sound init"

DEVICE=$(grep 'Class 0401:' /tmp/LSPCI.txt )
if [ "${DEVICE}" != "" ]; then
  ID=$(echo ${DEVICE} | cut -d ' ' -f4)
  echo ID=\"${ID}\"

  if   [ "${ID}" = "1106:3058" ]; then
    # Older VIA chip set (Kontron 400Mhz?)
    DRIVERLIST="snd snd-timer snd-pcm snd-seq-device snd-ac97-codec "
    DRIVERLIST="${DRIVERLIST} snd-rawmidi snd-mpu401-uart "
    DRIVERLIST="${DRIVERLIST} snd-via82xx "
    DRIVERLIST="${DRIVERLIST} snd-mixer-oss snd-pcm-oss "
    DRIVERPATH="/lib/modules/2.4.18-3pt/misc-o"
  elif [ "${ID}" = "1106:3059" ]; then
     # Newer Kontron 300MHz 
     DRIVERLIST="snd-page-alloc snd "
     DRIVERLIST="${DRIVERLIST} snd-timer snd-pcm snd-seq-device snd-ac97-codec "
     DRIVERLIST="${DRIVERLIST} snd-rawmidi snd-mpu401-uart "
     DRIVERLIST="${DRIVERLIST} snd-via82xx "
     DRIVERLIST="${DRIVERLIST} snd-mixer-oss snd-pcm-oss "
     DRIVERPATH="/lib/modules/2.4.18-3pt/misc-n"
  elif [ "${ID}" = "8086:24c5" ]; then
    # Intel
    DRIVERLIST="snd-page-alloc snd "
    DRIVERLIST="${DRIVERLIST} snd snd-timer snd-pcm snd-seq-device snd-ac97-codec "
    DRIVERLIST="${DRIVERLIST} snd-rawmidi snd-mpu401-uart "
    DRIVERLIST="${DRIVERLIST} snd-intel8x0 "
    DRIVERLIST="${DRIVERLIST} snd-mixer-oss snd-pcm-oss "
    DRIVERPATH="/lib/modules/2.4.18-3pt/misc-n"
  else
    echo UNKOWN SOUND DEVICE ID=\"${ID}\" 
  fi

  # all sound cards need this loaded
  modprobe soundcore

  for MOD in ${DRIVERLIST}; do
    MODPATH=$(find ${DRIVERPATH} -name ${MOD}.o 2>/dev/null)
    if [ "${MODPATH}" != "" ]; then
      insmod ${MODPATH} >>/tmp/rc.sysinit.log
    else
      RESULTS="${RESULTS} : sound missing ${MOD}"
    fi
  done
fi

export RESULTS

