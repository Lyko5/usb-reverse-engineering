#!/bin/sh

export INITFROM=$(dirname "${0}")

echo \"${INITFROM}\"

/sbin/lspci -n -v >/tmp/LSPCI.txt

export RESULTS=""

. ${INITFROM}/ethernet-init.sh

. ${INITFROM}/sound-init.sh

. ${INITFROM}/usb-init.sh

echo RESULTS=\"${RESULTS}\"

if [ "${RESULTS}" != "" ]; then exit 1; fi
exit 0

300MHz

00:00.0 Class 0600: 1106:0605
00:01.0 Class 0604: 1106:8605
00:02.0 Class 0200: 1282:9102 (rev 40)
00:07.0 Class 0601: 1106:0686 (rev 40)
00:07.1 Class 0101: 1106:0571 (rev 06)
00:07.2 Class 0c03: 1106:3038 (rev 1d)
00:07.3 Class 0c03: 1106:3038 (rev 1d)
00:07.4 Class 0601: 1106:3057 (rev 40)
00:07.5 Class 0401: 1106:3058 (rev 50)
00:08.0 Class 0300: 10de:0322 (rev a1)
00:09.0 Class 0700: 13a8:0158 (rev 09)
01:00.0 Class 0300: 5333:8d01 (rev 02)

1GHz

00:00.0 Class 0600: 8086:3580 (rev 02)
00:00.1 Class 0880: 8086:3584 (rev 02)
00:00.3 Class 0880: 8086:3585 (rev 02)
00:02.0 Class 0380: 8086:3582 (rev 02)
00:02.1 Class 0380: 8086:3582 (rev 02)
00:1d.0 Class 0c03: 8086:24c2 (rev 02)
00:1d.1 Class 0c03: 8086:24c4 (rev 02)
00:1d.7 Class 0c03: 8086:24cd (rev 02)
00:1e.0 Class 0604: 8086:244e (rev 82)
00:1f.0 Class 0601: 8086:24c0 (rev 02)
00:1f.1 Class 0101: 8086:24cb (rev 02)
00:1f.3 Class 0c05: 8086:24c3 (rev 02)
00:1f.5 Class 0401: 8086:24c5 (rev 02)
02:02.0 Class 0680: 1283:8888 (rev 03)
02:05.0 Class 0300: 10de:0322 (rev a1)
02:08.0 Class 0200: 8086:103a (rev 82)



