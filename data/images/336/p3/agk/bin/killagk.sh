#!/bin/ash
#
# functions  This file will shut down the AGK programs
#            and make sure they are really gone.
#
# Author:    T.E. Buckeyne, <TB@sierradesign.com>
#

# make sure we have "root" priviledges.. many of our task require it...
uid=$(id -u)
if [ $uid -ne 0 ] ; then
  echo 'not root priviledges => recursing...'
  setuid root $0 $1 $2 $3 $4 $5 $6
  exit
fi
unset uid

if [ -n "$1" ] ; then
  Haveone="true"
else
  Haveone="false"
fi

while [ $Haveone = "true" ] ; do

    for sig in TERM STOP CONT TERM KILL ; do

        Haveone="false"

        for task in $1 ; do
          pid=$(/sbin/pidof $task)
          if [ -n "$pid" ] ; then
#	      echo 'killall -'"$sig" "$task"
              killall -$sig "$task"
              Haveone="true"
          fi
        done

	#wait for him to die
	usleep 1000

    done

    if [ $Haveone = "true" ] ; then
        echo ' sleep and retry '
        usleep 1000
    else
      shift 1
      if [ -n "$1" ] ; then
        Haveone="true"
      fi
    fi
done

