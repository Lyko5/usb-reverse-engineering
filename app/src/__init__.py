import usb.core

dev = usb.core.find(idVendor=0x0424, idProduct=0x2514)

if dev is None:
    raise Exception('Device not found')

reattache = False
if dev.is_kernel_driver_active(0):
    reattach = True
    dev.detach_kernel_driver(0)

dev.set_configuration()

