BEGIN { }
{
	if ( NF == 1 ) {
		path=$0
		sub(/:/,"/",path)
	}

   astring=$1
   bstring = substr(astring, 1, 1)
   if ( bstring != "l" ) {
		next
	}

	if ( NF > 10 ) {
		size=length($9)
		tstring=substr($9,size)
		if ( tstring != "/" ) {
        	printf("%s%s\0%s\0",path,$9,$11)
		}
	}

}

