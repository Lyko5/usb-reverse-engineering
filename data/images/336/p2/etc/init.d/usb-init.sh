#!/bin/sh

# 300MHz    00:07.2 Class 0c03: 1106:3038 (rev 1d)

# 1GHz      00:1d.0 Class 0c03: 8086:24c2 (rev 02)
#           00:1d.7 Class 0c03: 8086:24cd (rev 02)

# find the usb devices

echo "starting usb init"

DEVICE=$(grep ' 0c03:' /tmp/LSPCI.txt )
if [ "${DEVICE}" != "" ]; then
  ID=$(echo ${DEVICE} | cut -d ' ' -f3)
  echo ID=\"${ID}\"

  if   [ "${ID}" = "1106:3038" ]; then
    # Kontron 300MHz 
    DRIVERLIST="usbcore uhci-hcd"
  elif [ "${ID}" = "8086:24c2" ]; then
    # Intel

    # DRIVERLIST used to include "usbcore input hid usb-uhci ehci-hcd", but 
    # "hid" and "input" were masking our usb devices preventing 
    # auto-configuration, while "ehci-hcd" is currently hanging/crashing 
    # the system.

    DRIVERLIST="usbcore uhci-hcd"

  else
    echo UNKOWN USB DEVICE ID=\"${ID}\" 
  fi
  for MOD in ${DRIVERLIST}; do
    MODPATH=$(find /lib/modules/$UNAME/kernel/drivers -name ${MOD}.ko 2>/dev/null)
    if [ "${MODPATH}" != "" ]; then
      insmod ${MODPATH} >>/tmp/rc.sysinit.log
    else
      RESULTS="${RESULTS} : usb missing ${MOD}"
    fi
  done
fi

# lsusb uses the usbfs or usbdevfs file system
if   [ "$(grep usbfs /proc/filesystems)" != "" ];then
  mount -nt usbfs none /proc/bus/usb
elif [ "$(grep usbdevfs /proc/filesystems)" != "" ];then
  mount -nt usbdevfs none /proc/bus/usb
fi

# we don't want to hold up initalization while 
# usb discovers all of its devices so use a function
# and launch it in the background

LOADUSB ()
{
local COUNT ; local LAST
let COUNT=1 ; let LAST=0

# keep getting /tmp/LSUSB.txt until all devices are found
  while [ ${COUNT} -ne ${LAST} ]; do
    let LAST=${COUNT}
    sleep 2
    /sbin/lsusb -v | grep "Bus" >/tmp/LSUSB.txt
    let COUNT=$(( $(wc -l /tmp/LSUSB.txt | cut -d '/' -f1) ))
    echo LOADUSB: LAST=${LAST} COUNT=${COUNT}
  done

# now that /tmp/LSUSB.txt is stable we can load other drivers
local DRIVER=""
local DATA[0]=""
local MOD=""
  
  while [ 1 ]; do
  
    read -a DATA; let R=$?
    if [ ${R} -ne 0 ]; then break; fi
  
    echo -n "LOADUSB ${DATA[5]} "
    case ${DATA[5]} in
      0000:0000) # hub nothing to do
         echo HUB
         ;;
      0403:6001) # serial device
         for (( i=0 ; i<=7; i++ ));do
             rm -f /dev/ttyUSB${i}
             mknod --mode 0666 /dev/ttyUSB${i} c 188 ${i}
         done
         unset i
         DRIVER="${DRIVER} usbserial ftdi_sio"
         echo SERIAL
         ;;
      0914:ff00) # serial device
         for (( i=0 ; i<=7; i++ ));do
             rm -f /dev/ttyUSB${i}
             mknod --mode 0666 /dev/ttyUSB${i} c 188 ${i}
         done
         unset i
         DRIVER="${DRIVER} usbserial ftdi_sio"
         echo SERIAL
         ;;
      *)         # we don't know what this is
         echo UNKNOWN DEVICE
         ;;
    esac
  
  done </tmp/LSUSB.txt
  
  for MOD in ${DRIVER}; do
    echo load ${MOD}
    echo load ${MOD} >>/tmp/rc.sysinit.log
    modprobe ${MOD } >>/tmp/rc.sysinit.log
  done

  unset -v MOD DRIVER DATA COUNT LAST
}

# in the future we can load any extra USB drivers from /tmp/LSUSB.txt
#Right now not invoking the LOADUSB function can be enabled on need basis
#LOADUSB >/tmp/loadusb.log &

export RESULTS

