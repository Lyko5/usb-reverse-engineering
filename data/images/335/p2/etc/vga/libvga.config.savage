# Configuration file for svgalib. Default location is /etc/vga.
# Other config file locations:	~/.svgalibrc
# 				where SVGALIB_CONFIG_FILE points
# Lines starting with '#' are ignored.

# If you have two vga cards with the same pci vendor id, svgalib will try
# to use the first one, even if the second one is active. In that case,
# use PCIStart to force starting the search for a vga card only at a
# specific bus and device numbers. For example, an AGP card is usually on
# bus 1, while pci is on bus 0, so to use the AGP card, uncomment:

# PCIStart 1 0

# Have a deep look at README.config to see what can do here (especially
# for mach32).

kbd_only_root_keymaps

# kbd_fake_mouse_event, as it says, sends a fake mouse event to the program.
# The format is: kbd_fake_mouse_event scancode [flag(s)] command [argument]
#   Scancode is a raw scancode or a descriptive name, the same as with fake
#   keyboard events (see above). If you use keymap conversion, specify
#   scancodes for the keyboard layout the program will receive.
#   Flags:   down   - trigger event when the key is pressed (default)
#	     up     - the opposite
#	     both   - trigger in both case, if pressed/released
#	     repeat - repeat events if the key is kept pressed (off by default)
#   commands: delta[xyz]  - send a fake delta event as if you have moved your
#	 		    mouse. If the parameter is 'off' / 'on' it will turn
#			    off/on the respective mouse axis (requires a
#			    parameter, of course)
#	      button[123] - send a fake event that the mouse button is pressed
#			    or released that's given by the parameter.
# 			    ('pressed' or 'released')
# Here are some examples:

#  This is one I use in *uake: it turns around, looks down a bit and when the
#  key is released it does the opposite, so it gets back to the starting state.
#  With this one and the help of a rocket you can fly though the whole map :)
#  (Scancode 28 is enter)
# kbd_fake_mouse_event 28 both deltax 8182 down deltay -1500 up deltay 1500

#  This one will switch off the y axis of the mouse while the key (right ctrl)
#  is kept pressed.
# kbd_fake_mouse_event 97 down deltay off  up deltay on

#  This one is the same as if you were pressing the left mouse button. (But
#  if you move your mouse then the button state will reset even if you keep
#  right ctrl down...)
# kbd_fake_mouse_event 97 down button1 pressed up button1 released

# Monitor type:

# Only one range can be specified for the moment.  Format:
# HorizSync min_kHz max_kHz
# VertRefresh min_Hz max_Hz

# Typical Horizontal sync ranges
# (Consult your monitor manual for Vertical sync ranges)
#
# 31.5 - 31.5 kHz (Standard VGA monitor, 640x480 @ 60 Hz)
# 31.5 - 35.1 kHz (Old SVGA monitor, 800x600 @ 56 Hz)
# 31.5 - 35.5 kHz (Low-end SVGA, 8514, 1024x768 @ 43 Hz interlaced)
# 31.5 - 37.9 kHz (SVGA monitor, 800x600 @ 60 Hz, 640x480 @ 72 Hz)
# 31.5 - 48.3 kHz (SVGA non-interlaced, 800x600 @ 72 Hz, 1024x768 @ 60 Hz)
# 31.5 - 56.0 kHz (high frequency, 1024x768 @ 70 Hz)
# 31.5 - ???? kHz (1024x768 @ 72 Hz)
# 31.5 - 64.3 kHz (1280x1024 @ 60 Hz)

HorizSync   30.0  96.0
VertRefresh 50.0  85.0

# Modes can be specified in two formats.  A compact one-line format, or
# a multi-line format.
# Montitor timings
#
# These are prefered over the default timings (if monitor and chipset
# can handle them). Not all drivers use them at the moment, and Mach32
# has its own syntax (see below).
# The format is identical to the one used by XFree86, but the label
# following the modeline keyword is ignored by svgalib.
#
# Here some examples:

# modeline "640x480@100"  43  640  664  780  848   480  483  490  504
# modeline "1024x768@75"  85.2 1024 1056 1376 1408  768 782 792 807
modeline "1360x768@75"  85.5 1360 1424 1536 1792  768 771 777 795

# My Test Modeline Below
# modeline "800x600@56" 35.27 800 832 960 992 600 612 618 631

# It seems there is a need for a 512x384 mode, this timing was donated
# by Simon Hosie <gumboot@clear.net.nz>: (it is 39kHz horz by 79Hz vert)

# Modeline "512x384@79"     25.175 512  522  598  646   384  428  436  494

# Here's a 400x300 Modeline (created by svidtune). Note that for
# doublescan modes, the Vertical values are half the real one (so XFree86
# modelines can be used). 

# Modeline "400x300@72" 25.000 400 440 504 520 300 319 322 333 doublescan

# Here is a mode for a ZX Spectrum emulator:
# Modeline "256x192@73" 12.588 256 269 312 360 192 208 212 240 doublescan
# newmode 256 192 256 256 1

# the width must be divisible by 8. Some cards require even divisiblity by
# 16, so that's preferable, since there are no standard modes where the
# width is not divisible by 16.

# The following modes are defined in svgalib, but have no timings in
# timing.c, so you'll have to add a modeline in order to use them:
# 1280x720, 1360x768, 1800x1012, 1920x1080, 1920x1440, 2048x1152
# and 2048x1536   

# Mach32 timings:

# e.g. Setup a 320x200 mode for the mach32:

#define 320x200x32K 320x200x64K 320x200x16M 320x200x16M32
#                     16 320 392 464 552 200 245 265 310

# These are REQUIRED for above mode, please edit to suit your monitor.
# (No, I won't pay for a new one)
# HorizSync 29 65
# VertRefresh 42 93.5

# Chipset type:
#
# Use one of the following force chipset type.
# Autodetects if no chipset is specified.
#
# If you have a PCI or AGP card, don't use chipset type forcing.
# If the card is not autodetected, its a bug, and it will probably
# not work even with forcing. Try running vgatest (with no chipset 
# line), and send to me (matan@svgalib.org) the output, a copy of
# /proc/pci (or lspci -n -vv) and whatever info you have on the card. 
#
# If a chipset driver gives trouble, try forcing VGA.

# chipset VGA		# Standard VGA
# chipset EGA		# EGA
# chipset ET3000	# Tseng ET3000
# chipset ET4000	# Tseng ET4000
# chipset Cirrus	# Cirrus Logic GD542x
# chipset TVGA		# Trident TVGA8900/9000
# chipset Oak		# Oak Technologies 037/067/077
# chipset S3		# S3 chipsets
# chipset GVGA6400	# Genoa 6400
# chipset ARK		# ARK Logic
# chipset ATI		# old ATI VGA
# chipset Mach32	# ATI Mach32
# chipset ALI		# ALI2301
# chipset Mach64	# ATI Mach64 - deprecated
# chipset ET6000        # Tseng ET6000
# chipset APM	 	# Alliance Technology AT 24/25/3D
# chipset NV3		# nVidia Riva 128 / TNT / GeForce
# chipset VESA          # nicely behaved Vesa Bioses
# chipset MX		# MX86251 (some Voodoo Rush boards)
# chipset PARADISE 	# WD90C31
# chipset RAGE		# RagePro (and might work with some older mach64)
# chipset BANSHEE	# Banshee/V3.
# chipset SIS		# SiS 5597/6326/620/530 cards / integrated vga.
# chipset I740		# Intel i740 based cards.
# chipset FBDev		# Use the kernel frame buffer device for graphics
			# display.
# chipset G400		# Matrox G200/G400/G450
# chipset R128		# ATI Rage 128
  chipset SAVAGE	# S3 savage family
# chipset LAGUNA	# Cirrus Logic 546x (laguna) 


# EGA Color/mono mode:
# Required if chipset is EGA.
#
# Use one of the following digits to force color/mono:

# monotext  # Card is in monochrome emulation mode
# colortext # Card is in color emulation mode
colortext

# RAMDAC support:
# Some chipsets (e.g. S3 and ARK) allows specifying a RAMDAC type.
# If your RAMDAC is not autodetected, you can try specifying it.
# Do NOT specify a RAMDAC if you card uses the S3 Trio chipset
# (the RAMDAC is built in).

# Ramdac Sierra32K
# Ramdac SC15025
# Ramdac SDAC         # S3 SDAC
# Ramdac GenDAC       # S3 GenDAC
# Ramdac ATT20C490    # AT&T 20C490, 491, 492 (and compatibles)
# Ramdac ATT20C498    # AT&T 20C498
# Ramdac IBMRGB52x    # IBM RGB524, 526, 528 (and compatibles)

# Dotclocks:
# Some chipsets needs a list of dot clocks for optimum operation.  Some
# includes or supports a programmable clock chip.  You'll need to specify
# them here.

# Fixed clocks example:
# (The following is just an example, get the values for your card from
#  you XF86Config)

# Clocks 25.175 28.3 40 70 50 75 36 44.9 0 118 77 31.5 110 65 72 93.5

# Programmable clockchip example:

# Clockchip ICD2061A  # The only one supported right now



# Here are miscellaneous options to help with specific problems.

# VesaText	      # Helps the VESA driver with text mode restoration
		      # problems.

# VesaSave 14	      # changing value might help text mode restoring
		      # problems with VESA driver. Legal values: 0-15

# NoVCControl	      # Disables svgalib's finding a new VC if run
		      # from X. Good fo using dumpreg under X, but
		      # probably bad for standard usage.


# RageDoubleClock     # If your card is based on ATI's rage card, and
		      # the pixel clock is double what it should be 
		      # (main symptom is some modes are out of sync),
		      # try enabling this. If it helps, please report to
		      # me (matan@svgalib.org) 
