#! /bin/ash

# DHCP lookup an IP address for for the eth0 interface
# exit status indicates any errors that may have occured
# 0 - successfully looked up an IP address
# 1 - unable to obtain IP address before timeout
# 2 - eth0 has already acquired an IP address

export PATH=/bin:/usr/bin:/sbin:/usr/sbin
IPADDR_RE='s/ *inet addr:\([1-9][0-9.]*\).*/\1/p'

NetworkUp=$(ifconfig eth0 | grep UP)
Count=0

if [ -n "$NetworkUp" ]
then
  echo Network already assigned an IP address
  sleep 3
  exit 2
fi

/etc/init.d/network start

echo -n "Wait for network to come up." 

while [ \( -z "$NetworkUp" \) -a \( "$Count" -lt 15 \) ]
do
  echo -n "."
  sleep 1
  NetworkUp=$(ifconfig eth0 | grep UP)
  Count="$(( $Count + 1 ))"
done
echo ""

if [ -n "$NetworkUp" ]
then
  echo eth0 started up
  portmap
  xinetd
  AGKIP=$(ifconfig eth0 | sed -n -e "$IPADDR_RE")
  echo eth0 IP Address: "$AGKIP"
  exit 0
else
  echo eth0 startup failure
  killall udhcpc
  ifconfig eth0 down
  exit 1
fi
