#           FaultDog start / restart scripts
# fields:
# <valid mask> <script file name> <description>
#
 0x00000000 run_agk_clear    "Start Clear Chip"
 0xffffffff run_again_clear  "Restart Clear Chip"
#
# mask = 0x00000000 valid for initial startup only
#        0x00000010 valid for software fault
#        0xffffffff valid for all fault types (development only)
#
# format is fixed field order, spaces mandatory, 
#   quoted description, and no optional fields
