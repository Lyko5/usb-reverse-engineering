#!/bin/sh
# script to start the simulated bill val in an exterm window

# Currency setup is as follows:
#   /agk/bin/simbillval $@ [currency_string]
# where $@ is server name and currency_string is one of these:
#
# en_US    U.S.
# ru_RU    Russia
#
# no currency_string will default to U.S.

if [ X"$AGKSIM" = X"-s" ]; then

xterm -geometry 59x16+$AGKVIDEOWIDTH+0 -e /agk/bin/simbillval $@


else

/agk/bin/jcmbillval $@

fi


