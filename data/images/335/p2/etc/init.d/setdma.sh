#!/bin/sh

if [ "$(echo $1 | grep '/dev/hd')" != "$1" ]; then echo invalid parameter 1; exit 0; fi

# get all of the dma modes the flash supports
FLASHDMA=$(echo $(/sbin/hdparm -i "$1" | grep 'DMA '))

# find the fastest mode supported
for TMODE in $FLASHDMA; do 
  if [ "$(echo $TMODE |grep 'dma')" != "" ]; then 
    MODE=$(echo $TMODE |sed -e "s/\*//")
    if [ "$(echo $MODE |grep '*')" != "" ]; then break; fi
  fi ; 
done
FLASHDMA=$MODE

# echo 'FLASHDMA     ='\"$FLASHDMA\"

if [ "$FLASHDMA" = "" ]; then echo flash does not support dma; exit 0; fi

ENABLEDMA="no"
let CTRLRDMA=$(echo $(/sbin/hdparm $1 | grep ' using_dma' | sed -e "s/.*=//" | sed -e "s/(.*)//"))
# echo CTRLRDMA     =$CTRLRDMA 

 
# echo 'AGKBRDREV    ='$AGKBRDREV 
# echo 'AGKMFG       ='$AGKMFG 

case $AGKMFG in

  # RESERVED
  0) ;;
  
  # SDG
  1)
    if [ "$AGKBRDREV" -ge 9 ]; then
      ENABLEDMA='yes'
    fi 
    ;;
  
  # IGT retrofit
  2) 
    ;;
  
  # BALLY
  3)
    # get the cabinet type 
    BCAB=$(( $AGKBRDREV / 32 ))
    BREV=$(( $AGKBRDREV - ( $BCAB * 32 ) ))
    case $BCAB in
      # 8700
      0)
        if [ "$BREV" -gt 3 ]; then
          ENABLEDMA='yes'
        fi
        ;;
      # Hybrid
      4) ENABLEDMA='yes' ;;
      # S6000
      6) ENABLEDMA='yes' ;;
      # any new cabinets will support DMA
      *) ENABLEDMA='yes' ;;
    esac
    ;;
  
  # special or unknown VT100 (no data)
  255) 
    ;;
  
  # In theory this can not happen, that is why we handle it!
  *) echo 'Unknown Manufacturer'$AGKMFG 
    ;;

esac

if [ "$ENABLEDMA" = "yes" ]; then
  echo "============================"
  echo "ENABLING DMA as $FLASHDMA"

  MODENUM="0"
  case $FLASHDMA in
    # test for the multi word dma modes
    mdam0) MODENUM="32" ;;
    mdma1) MODENUM="33" ;; 
    mdma2) MODENUM="34" ;;
    mdma3) MODENUM="35" ;;
    mdma4) MODENUM="36" ;;

    # test for the ultra dma modes
    udma0) MODENUM="64" ;;
    udma1) MODENUM="65" ;;
    udma2) MODENUM="66" ;;
    udma3) MODENUM="67" ;;
    udma4) MODENUM="68" ;;
    udma5) MODENUM="69" ;;
        *) MODENUM="0"  ;;
  esac
  
  if [ "$MODENUM" != "0" ]; then
    /sbin/hdparm -X $MODENUM "$1"
    /sbin/hdparm -d 1 "$1"
  fi
  echo "============================"
else
  echo "============================"
  echo "LEAVING DMA DISABLED"
  echo "============================"
fi

# /sbin/hdparm $1
# /sbin/hdparm -i $1
