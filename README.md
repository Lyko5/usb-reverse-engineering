# Reverse Engineering Bally Slot Machine

Working to Reverse Engineer the Reel Controller (AVRU6REEL188-03) on my Bally Alpha S9000 Transmissive 5-Reel Slot Machine running Blazing Stars with Hot and Wild features

Copy-Pasta of [Reddit Post](https://www.reddit.com/r/Hacking_Tutorials/comments/evwez7/attempting_to_reverse_engineer_usb_device/):

> I'm in a bit over my head, I'm usually a web developer and this is my first time working with hardware.
>
> I have two proprietary devices, one is basically a computer running hardened Linux (MPU), the other has some lights and motors and is connected to the MPU via USB.
> I'd like to reverse engineer the USB device and write Python to control the lights with a raspberry pi without the MPU.
>
> I have already been sniffing the USB traffic with a hardware sniffer (https://www.bugblat.com/products/ezsniff/index.html) and have the output.
> This includes control transfers like Get Descriptor, Set Address, Get Status, Set Feature along with the raw data that is getting transferred.
> 
> I have been looking a documentation with pyusb on GitHub and how to emulate the commands.
> I kinda get it but not really.
> I am at the point where the device acknowledges it is connected; flashing status light goes solid after I run a set configuration command with my python but that's it.
>
> Does anyone have any insight or pointers on where I should go next? Should I upload my code and the sniffing result?

## More Background

This machine was built 2010 by Bally Gaming.
There are not many transmissive display games because of a legal battle between Bally and WMS.

I have looked all over the internet and there is very little documentation seeing as this is a slot machine.

There is the Main Processing Unit (MPU), basically a not-so-beefy computer that runs a hardened linux os.
Connected to it is all sorts of things, dual touch screens, push buttons, external lights, speakers, Compact Flash cards for data.
I read the CF card and there is basically a linux os and I'm not sure where to look for a driver.

Additionally, the Reel Controller (RC) is connected to the MPU by USB.
This is the device that I would like to be able to connect to a RaspberryPi and control.
Not sure what USB version it is using, definately not 3.

On the main RC board, there are 3 more USB ports.
These ports can have other items plugged in and communicate to the MPU.
Example: 7 segment display.

This is a five reel slot.
Each reel has it's own microstepper motor, sensors, lights, and reel strips.
I do not know how many steps, but they have sensors to determine the positions.
There are 22 positions per motor, about 9 steps apart each.
I have only ever seen the reels spin in one direction, I know some games exist that can spin the reels backwards for features.

In addition to the 5 motors, each reel has a panel of LEDs.
There are three sections, and in each section has 5 lights, red, green, blue, and two whites.
Each color per section can be turned on individually.
I guess 60 different individual components (5 boards x 3 sections x 4 segments) that are controlled just for the lights.

From the data that I caputred already, the RC is acting as a hub (device type 9) in addition to it's own functions.

The machine is fully working and I have all the parts and can plug it all back in again and do more specific scanning or tests.

## Repo Information

### documents.md
This is a list of resources that I have been trying to use and understand as well as the sniffer page with info on using it and reading the output data.

### data
Various data that I have gatnered.

#### images
Using ```dd``` I was able to copy most of the data from the compact flash cards.

The 335 directory is the Clear/Reset card that, if booted to, clears the machines NVRAM.
There were 6 partitions
4 was weird, just another two .img files and 6 was all empty directories.

The 336 directory is the OS card.

The ATGBZST02600-01 directory is the game card.

### sniffData
I got the sniff information using the ezSniffer following the instructions on their page.

#### 5MotorSpinWithStops.bud
This is a recording of the 5 reels spinning an unknown rotation to stop at specific points.
Just a basic spin.

All reels started at the same time, then the first one stopped, second, third, fourth, and last.

Lights also flashed in an unknown pattern during recording.

#### Calibrate.bud
This is a recording of the MPU sending a calibrate command.

The five reels started from unknown positions, rotated and unknown amount, stopped at their respective 'home' or 0 positions.

I believe they then returned to their positions before the calibrate command.

#### DeviceInitilize.bud
This is a recording of the RC being connected to the MPU and the initial power-on.

#### FlashC1D1E0.bud
This is a recording of light positions C1 D1 and E0 flashing.
I don't remember what colors specifically.

#### LightTestSequence.bud
This is a recording of the MUP sending a self light test command.

This command lights all LEDs, then all per color.

Color order is currently unknown.
