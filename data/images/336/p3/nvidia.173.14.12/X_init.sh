#!/bin/sh

#PATH=/usr/X11R6/bin:$PATH:
export DISPLAY=":0.0"

#nvidia_drv takes care of inserting the nvidia.ko kernel module
#These need to verified with nVidia
#if [ "$(lsmod | grep "^nvidia")" = "" ]; then
#  /sbin/insmod -f /lib/modules/2.4.18-3pt/nvidia/nvidia.o
#fi

#if [ "$(lsmod | grep "^agpgart")" = "" ]; then
#  /sbin/insmod -f /initrd/startup/agpgart.o
#fi

rm -f /tmp/.X0-lock

if [ "$(pidof xfs)" = "" ]; then
  /usr/bin/xfs -droppriv -daemon 
#  usleep 500000
fi

if [ "$(pidof Xorg)" = "" ]; then
  # Because -screen & -layout don't seem to mix
  if [ -z $Screen ]; then
    # For different resolution configurations , use -layout
    /usr/bin/Xorg -config /tmp/XF86Config-4 -layout $XLayout \
       -modulepath /usr/lib/xorg/modules,/usr/X11R6/lib/xorg/modules -dpms -s 0 \
       -depth $Depth  vt03  -logverbose 255 -logfile /tmp/x.log -allowMouseOpenFail -noreset &
  else
    # For regular/rotated/TwinView configurations , use -screen
    /usr/bin/Xorg -config /tmp/XF86Config-4  -screen $Screen \
       -modulepath /usr/lib/xorg/modules,/usr/X11R6/lib/xorg/modules  -dpms -s 0 \
       -depth $Depth vt03   -logverbose 255 -logfile /tmp/x.log -allowMouseOpenFail -noreset &
  fi
  
fi

sleep 1

#/usr/bin/xset -dpms
#/usr/bin/xset s off

# remove the X crosshair in the center of the screen
/usr/bin/xsetroot -cursor /etc/X11/emptycursor /etc/X11/emptycursor

